export class MockRoll20Object implements Roll20Object {
  private props: { [property: string]: any };

  constructor(type: ObjectType, id: string, properties: { [property: string]: any } | undefined) {
    this.props = { _id: id, _type: type, ...properties };
  }

  get id(): string {
    return this.props['_id'];
  }

  get _id(): string {
    return this.props['_id'];
  }

  get _type(): ObjectType {
    return this.props['_type'];
  }

  get(property: string): any {
    return this.props[property];
  }

  remove(): void {
    return;
  }

  set(property: string, value: any): void {
    if (property.startsWith('_')) throw new Error('cannot set immutable properties');

    const oldValue = this.props[property];
    if (oldValue !== undefined) this.props[property] = value;
  }
}
